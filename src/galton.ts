import Chart from "chart.js";

// https://www.dur.ac.uk/brand/local/colourpalette/
const style: CSSStyleDeclaration = getComputedStyle(document.body);
const dc = {
    purple: style.getPropertyValue("--purple"),
    yellow: style.getPropertyValue("--yellow"),
    cyan: style.getPropertyValue("--cyan"),
    red: style.getPropertyValue("--red"),
    gold: style.getPropertyValue("--gold"),
    heather: style.getPropertyValue("--heather"),
    stone: style.getPropertyValue("--stone"),
    sky: style.getPropertyValue("--sky"),
    cedar: style.getPropertyValue("--cedar"),
    concrete: style.getPropertyValue("--concrete"),
    ink: style.getPropertyValue("--ink"),
    black: style.getPropertyValue("--black"),
    tr_heather: style.getPropertyValue("--tr-heather"),
    tr_concrete: style.getPropertyValue("--tr-concrete"),
    primary: style.getPropertyValue("--primary"),
    background: style.getPropertyValue("--background"),
    background2: style.getPropertyValue("--background2"),
}

const canvas = document.getElementById('simulation') as HTMLCanvasElement;
const context = canvas.getContext('2d', { alpha: false, })!;

const static_canvas = document.createElement('canvas') as HTMLCanvasElement;
const static_context = static_canvas.getContext('2d', { alpha: false })!;

const chart_canvas = document.getElementById('chart') as HTMLCanvasElement;
const chart_context = chart_canvas.getContext('2d')!;

let dt: number;
const scale: number = 1;
const g: number = 20;
const friction: number = 0.9;

const triangle_base: number = 38 * scale;
const triangle_height: number = Math.sqrt(3) / 2 * triangle_base;
const num_pinboard_levels: number = 13;

const collector_top: number = 3 * triangle_height + triangle_height * (num_pinboard_levels - 1) + triangle_height;
const collector_bottom: number = collector_top + 2 * triangle_height;

canvas.width = triangle_base * (num_pinboard_levels + 1);
canvas.height = collector_bottom;

static_canvas.width = canvas.width;
static_canvas.height = canvas.height;
static_context.fillStyle = dc.background;
static_context.fillRect(0, 0, canvas.width, canvas.height);

chart_canvas.width = canvas.width;
chart_canvas.height = 300;

// Histogram

let ball_count: number = 0;

const chart_options: Chart.ChartOptions = {
    responsive: false,
    scales: {
        xAxes: [{
            gridLines: {
                drawOnChartArea: false,
                color: dc.primary,
            },
            scaleLabel: {
                labelString: 'Bin',
                display: true,
                fontColor: dc.ink,
            },
            ticks: {
                fontColor: dc.ink,
            }
        }],
        yAxes: [{
            gridLines: {
                drawOnChartArea: false,
                color: dc.primary,
            },
            ticks: {
                min: 0,
                precision: 0,
                fontColor: dc.ink,
            } as Chart.LinearTickOptions,
            scaleLabel: {
                labelString: 'Count',
                display: true,
                fontColor: dc.ink,
            },
        }],
    },
    legend: {
        display: false,
    },
    title: {
        display: true,
        text: 'Total ball count: ' + ball_count,
        position: 'bottom',
        fontColor: dc.primary,
        // fontStyle: 'regular',
    },
    tooltips: {
        enabled: true,
        displayColors: false,
        backgroundColor: dc.tr_concrete,
        titleFontColor: dc.primary,
        bodyFontColor: dc.primary,
        callbacks: {
            title: (items: Chart.ChartTooltipItem[]): string => { return 'Bin ' + items[0].xLabel; },
        },
    },
}

const chart_data: Chart.ChartData = {
    labels: [...Array(num_pinboard_levels).keys()].map(i => i + 1),
    datasets: [{
        label: 'Count',
        barPercentage: 1,
        categoryPercentage: 1,
        backgroundColor: dc.background2,
        hoverBackgroundColor: dc.sky,
        data: Array(num_pinboard_levels).fill(0),
    }],
}

const histo: Chart = new Chart(chart_context, {
    type: 'bar',
    data: chart_data,
    options: chart_options
});

// Galton board

const colours: Array<string> = [dc.yellow, dc.cyan, dc.red, dc.gold];

function random_int(max: number): number {
    return Math.floor(Math.random() * max);
}

function random_colour(): string {
    return colours[random_int(colours.length)];
}

function random_float(min: number, max: number): number {
    return Math.random() * (max - min) + min;
}

interface Proj {
    radius: number;
    x: number;
    y: number;
}

interface Pos {
    x: number;
    y: number;
}

class Circle {
    constructor(
        public x: number,
        public y: number,
        public radius: number,
        public colour: string,
    ) {
    }

    draw(ctx: CanvasRenderingContext2D) {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.fillStyle = this.colour;
        ctx.fill();
    }
}

class Ball extends Circle {
    delete: boolean = false;

    constructor(
        x: number,
        y: number,
        public vx: number,
        public vy: number,
        r: number,
    ) {
        super(x, y, r, random_colour());
    }

    collide(obstacle: Proj | Pin) {
        // https://en.wikipedia.org/wiki/Elastic_collision#Two-dimensional_collision_with_two_moving_objects
        // take limit of above where obstacle has mass >> mass of ball

        // test if ball collided with obstacle; if so, do collision
        const sep: Segment = new Segment(obstacle.x, obstacle.y, this.x, this.y, 0, "");
        const x0: number = this.radius + obstacle.radius;

        if (sep.length < x0) {
            // calculate and set velocity after collision
            const dot: number = this.vx * sep.nx + this.vy * sep.ny;
            this.vx -= 2 * dot * sep.nx;
            this.vy -= 2 * dot * sep.ny;

            // add friction
            this.vx *= friction;
            this.vy *= friction;

            // place ball at boundary (since it's currently slightly inside the obstacle)
            this.x = obstacle.x + sep.nx * x0;
            this.y = obstacle.y + sep.ny * x0;
        }
    }

    reflect(line: Segment) {
        // https://en.wikipedia.org/wiki/Reflection_%28mathematics%29#Reflection_across_a_line_in_the_plane
        // https://en.wikipedia.org/wiki/Vector_projection#Vector_projection_2

        // find point on line nearest to ball
        const dot: number = (this.x - line.x1) * line.nx + (this.y - line.y1) * line.ny;
        const proj: Proj = {
            radius: line.radius,
            x: line.x1 + dot * line.nx,
            y: line.y1 + dot * line.ny
        };

        // http://jeffreythompson.org/collision-detection/line-point.php
        // test whether nearest point actually lies within the ends of the line
        const d1: number = Math.hypot(proj.x - line.x1, proj.y - line.y1);
        const d2: number = Math.hypot(proj.x - line.x2, proj.y - line.y2);
        if (Math.abs(d1 + d2 - line.length) < 1) {
            this.collide(proj);
        }
    }

    update() {
        // https://en.wikipedia.org/wiki/Verlet_integration#Velocity_Verlet
        // TODO record last position to determine trajectory in collisions, or use velocity, to stop collision skipping
        this.x += dt * this.vx;
        this.y += dt * (this.vy + 0.5 * dt * g);
        this.vy += dt * g;

        // tag balls for deletion if they fall off the bottom
        if (this.y > canvas.height + this.radius) {
            this.delete = true;
            return;
        }

        for (const wall of quincunx.boundaries.inner_walls) {
            this.reflect(wall);
        }

        for (const wall of quincunx.collector.walls) {
            this.reflect(wall);
        }

        for (const barrier of quincunx.barriers) {
            this.reflect(barrier);
        }

        for (const pin of quincunx.pins) {
            this.collide(pin);
        }

    }

}

class Pin extends Circle {
    selected: boolean = false;

    constructor(x: number, y: number, r: number) {
        super(x, y, r, dc.primary);
    }

    inside(pos: Pos) {
        return Math.hypot(this.x - pos.x, this.y - pos.y) < pin_click_padding;
    }
}

class Line {
    radius: number;

    constructor(
        public x1: number,
        public y1: number,
        public x2: number,
        public y2: number,
        public width: number,
        public colour: string,
    ) {
        this.radius = this.width / 2;
    }

    draw(ctx: CanvasRenderingContext2D) {
        ctx.beginPath();
        ctx.moveTo(this.x1, this.y1);
        ctx.lineTo(this.x2, this.y2);
        ctx.closePath();
        ctx.strokeStyle = this.colour;
        ctx.lineWidth = this.width;
        // TODO get lineCap = 'round' to work
        // ctx.lineCap = 'round';
        ctx.stroke();
    }
}

class Segment extends Line {
    nx: number;
    ny: number;
    length: number;

    constructor(
        x1: number,
        y1: number,
        x2: number,
        y2: number,
        w: number,
        c: string,
    ) {
        super(x1, y1, x2, y2, w, c);
        this.nx = this.x2 - this.x1;
        this.ny = this.y2 - this.y1;
        this.length = Math.hypot(this.nx, this.ny);
        this.nx /= this.length;
        this.ny /= this.length;
    }

    recalc(): void {
        this.nx = this.x2 - this.x1;
        this.ny = this.y2 - this.y1;
        this.length = Math.hypot(this.nx, this.ny);
        this.nx /= this.length;
        this.ny /= this.length;
    }

    set_pos2(x: number, y: number): void {
        this.x2 = x;
        this.y2 = y;
    }
}

class Boundary {
    fill_colour: string = dc.background2;
    border_colour: string = dc.primary;

    constructor(
        public border_width: number,
        public x1: number,
        public y1: number,
        public x2: number,
        public y2: number,
        public x3: number,
        public y3: number,
        public x4: number,
        public y4: number,
        public x5: number,
        public y5: number,
        public x6: number,
        public y6: number,
        public x7: number,
        public y7: number,
    ) {
    }

    draw(ctx: CanvasRenderingContext2D) {
        ctx.beginPath();
        ctx.moveTo(this.x1, this.y1);
        ctx.lineTo(this.x2, this.y2);
        ctx.lineTo(this.x3, this.y3);
        ctx.lineTo(this.x4, this.y4);
        ctx.lineTo(this.x5, this.y5);
        ctx.lineTo(this.x6, this.y6);
        ctx.lineTo(this.x7, this.y7);
        ctx.closePath();

        ctx.fillStyle = this.fill_colour;
        ctx.fill();

        ctx.lineWidth = this.border_width;
        ctx.strokeStyle = this.border_colour;
        ctx.stroke();

    }
}

class Collector {
    left: number = triangle_base / 2;
    right: number = canvas.width - triangle_base / 2;
    last_bin: number = num_pinboard_levels - 1;
    bin_width: number = triangle_base;
    walls: Array<Segment> = [];

    constructor(public wall_width: number) {
        let x = this.left;
        while (x <= this.right) {
            this.walls.push(new Segment(x, collector_top, x, collector_bottom, wall_width, dc.primary));
            x += this.bin_width;
        }
    }

    index_bin(x: number) {
        if (x < this.left) {
            return 0;
        }
        if (x > this.right) {
            return this.last_bin;
        }
        return Math.floor((x - this.left) / this.bin_width);
    }

    draw(ctx: CanvasRenderingContext2D) {
        for (const wall of this.walls) {
            wall.draw(ctx);
        }
    }
}

class Boundaries {
    left: Boundary;
    right: Boundary;
    inner_walls: Array<Segment> = [];

    constructor(
        public border_width: number,
        public pinboard_extra_margin: number,
    ) {
        const middle: number = canvas.width / 2;

        const w1: number = canvas.width / 4; // funnel max radius
        const lx1: number = middle - w1;
        const rx1: number = middle + w1;
        const y1: number = 0;

        const w2: number = triangle_base / 2 + border_width / 2 + pinboard_extra_margin; // neck radius
        const lx2: number = middle - w2;
        const rx2: number = middle + w2;
        const y2: number = 2 * triangle_height; // funnel bottom (=neck top)

        const lx3: number = lx2;
        const rx3: number = rx2;
        const y3: number = 3 * triangle_height; // neck bottom

        const w4: number = triangle_base * (num_pinboard_levels - 1) / 2 + w2; // pinboard max radius
        const lx4: number = middle - w4;
        const rx4: number = middle + w4;
        const y4: number = y3 + triangle_height * (num_pinboard_levels - 1); // pinboard bottom

        const lx5: number = lx4;
        const rx5: number = rx4;
        const y5: number = collector_bottom;

        this.left = new Boundary(
            border_width,
            lx1,
            y1,
            lx2,
            y2,
            lx3,
            y3,
            lx4,
            y4,
            lx5,
            y5,
            0,
            collector_bottom,
            0,
            0,
        );

        this.right = new Boundary(
            border_width,
            rx1,
            y1,
            rx2,
            y2,
            rx3,
            y3,
            rx4,
            y4,
            rx5,
            y5,
            canvas.width,
            collector_bottom,
            canvas.width,
            0,
        );

        // funnel
        this.inner_walls.push(new Segment(lx1, y1, lx2, y2, this.border_width, ""));
        // neck
        this.inner_walls.push(new Segment(lx2, y2, lx3, y3, this.border_width, ""));
        // pinboard edge
        this.inner_walls.push(new Segment(lx3, y3, lx4, y4, this.border_width, ""));
        // gap between pinboard and collector (actually extends to bottom of canvas)
        this.inner_walls.push(new Segment(lx4, y4, lx5, y5, this.border_width, ""));

        // similarly on right hand side (symmetric)
        this.inner_walls.push(new Segment(rx1, y1, rx2, y2, this.border_width, ""));
        this.inner_walls.push(new Segment(rx2, y2, rx3, y3, this.border_width, ""));
        this.inner_walls.push(new Segment(rx3, y3, rx4, y4, this.border_width, ""));
        this.inner_walls.push(new Segment(rx4, y4, rx5, y5, this.border_width, ""));

    }

    draw(ctx: CanvasRenderingContext2D) {
        this.left.draw(ctx);
        this.right.draw(ctx);
    }
}

class Quincunx {
    balls: Array<Ball> = [];
    pins: Array<Pin> = [];
    barriers: Array<Segment> = [];

    ball_radius: number = 5 * scale;
    ball_max_v: number = 10;
    ball_insert_pos: number = triangle_height;

    extra: number;

    pins_top: number = 3 * triangle_height;
    pin_radius: number;

    wall_width: number = scale * 5.5;
    wall_radius: number = this.wall_width / 2;

    collector_wall_width: number;
    collector_wall_radius: number;

    boundaries: Boundaries;
    collector: Collector;

    constructor(extra_factor: number) {

        this.extra = extra_factor * scale;

        this.pin_radius = 1.75 * scale + this.extra;

        this.collector_wall_width = this.wall_width + this.extra;
        this.collector_wall_radius = this.collector_wall_width / 2;

        this.boundaries = new Boundaries(this.wall_width, this.extra);
        this.collector = new Collector(this.collector_wall_width);

        this.collector.draw(static_context);
        this.boundaries.draw(static_context);

        for (let i = 0; i < num_pinboard_levels + 1; i++) {
            for (let j = 0; j <= i; j++) {
                this.pins.push(new Pin(
                    canvas.width / 2 + triangle_base * (i / 2 - j),
                    triangle_height * i + this.pins_top,
                    i < num_pinboard_levels ? this.pin_radius : this.collector_wall_radius,
                ));
            }
        }

        for (const pin of this.pins) {
            pin.draw(static_context);
        }
    }

    add_balls(n: number) {
        for (let i = 0; i < n; i++) {
            this.balls.push(new Ball(
                canvas.width / 2,
                this.ball_insert_pos,
                random_float(-this.ball_max_v, this.ball_max_v),
                0,
                this.ball_radius,
            ));
        }
    }

    draw_and_update(ctx: CanvasRenderingContext2D) {
        for (const [i, ball] of this.balls.entries()) {
            if (ball.delete) {
                this.balls.splice(i, 1)
                const index: number = this.collector.index_bin(ball.x);
                ((histo.data.datasets!)[0].data![index]! as number) += 1;
                ball_count += 1;
                histo.options.title!.text = 'Total ball count: ' + ball_count;
                histo.update();
            }
            ball.draw(ctx);
            ball.update();
        }

        for (const barrier of this.barriers) {
            barrier.draw(ctx);
        }

    }

}

let quincunx: Quincunx = new Quincunx(0);

function draw(): void {
    context.drawImage(static_canvas, 0, 0);

    quincunx.draw_and_update(context);

    for (const highlight of highlights) {
        highlight.draw(context);
    }

    window.requestAnimationFrame(draw);
}

// UI

let pin_click_padding: number = 7 * scale;

const timestep = (document.getElementById('timestep') as HTMLInputElement)!;
const num_balls = (document.getElementById('num-balls') as HTMLInputElement)!;
const drop_balls = document.getElementById('drop-ball')!;
const reset = document.getElementById('reset')!;
const undo = document.getElementById('undo')!;
const exp = document.getElementById('exp')!;
const thry = document.getElementById('thry')!;

exp.addEventListener('click', (): void => {
    pin_click_padding = 7 * scale;
    static_context.fillStyle = dc.background;
    static_context.fillRect(0, 0, canvas.width, canvas.height);
    (histo.data.datasets!)[0].data!.fill(0);
    histo.update();
    quincunx = new Quincunx(0);
});

thry.addEventListener('click', (): void => {
    pin_click_padding = 14 * scale;
    static_context.fillStyle = dc.background;
    static_context.fillRect(0, 0, canvas.width, canvas.height);
    (histo.data.datasets!)[0].data!.fill(0);
    histo.update();
    quincunx = new Quincunx(10);
});

dt = timestep.valueAsNumber;

timestep.addEventListener('input', (event: Event): void => {
    const trgt = event.target as HTMLInputElement;
    dt = trgt.valueAsNumber;
});

let x: number = num_balls.valueAsNumber;
drop_balls.textContent = "Drop " + x + " ball" + (x > 1 ? "s" : "");

num_balls.addEventListener('input', (event: Event): void => {
    const trgt = event.target as HTMLInputElement;
    x = trgt.valueAsNumber;
    drop_balls.textContent = "Drop " + x + " ball" + (x > 1 ? "s" : "");
});

drop_balls.addEventListener('click', (): void => {
    quincunx.add_balls(x);
});

reset.addEventListener('click', (): void => {
    quincunx.balls.length = 0;
    quincunx.barriers.length = 0;
    highlights.length = 0;
    building = false;
    (histo.data.datasets!)[0].data!.fill(0);
    histo.update();
});

function position(cnvs: HTMLCanvasElement, event: MouseEvent): Pos {
    const rect: DOMRect = cnvs.getBoundingClientRect();
    const scale_x: number = cnvs.width / rect.width;
    const scale_y: number = cnvs.height / rect.height;

    return {
        x: (event.clientX - rect.x) * scale_x,
        y: (event.clientY - rect.y) * scale_y,
    }

}

let building: boolean = false;
let highlights: Array<Circle> = [];

canvas.addEventListener('mousemove', (event: MouseEvent): void => {
    const pos: Pos = position(canvas, event);

    for (const pin of quincunx.pins) {
        if (pin.inside(pos)) {
            if (!pin.selected) {
                pin.selected = true;
                highlights.push(new Circle(
                    pin.x,
                    pin.y,
                    pin_click_padding,
                    dc.sky,
                ))
            }
        } else {
            if (pin.selected) {
                pin.selected = false;
                highlights.pop();
            }
        }
    }

    if (building) {
        quincunx.barriers[quincunx.barriers.length - 1].set_pos2(pos.x, pos.y);
    }

});

canvas.addEventListener('click', (): void => {

    for (const pin of quincunx.pins) {
        if (pin.selected) {

            if (!building) {
                building = true;
                quincunx.barriers.push(new Segment(
                    pin.x,
                    pin.y,
                    pin.x,
                    pin.y,
                    quincunx.collector_wall_width,
                    dc.concrete,
                ));
            } else {
                pin.selected = false;
                highlights.pop();

                building = false;
                const i: number = quincunx.barriers.length - 1;
                quincunx.barriers[i].set_pos2(pin.x, pin.y);
                quincunx.barriers[i].recalc();
            }

        }
    }

});

undo.addEventListener('click', (): void => {
    if (building) {
        building = false;
    }

    quincunx.barriers.pop();

});

draw();
