import commonjs from '@rollup/plugin-commonjs';
import execute from 'rollup-plugin-execute';
import resolve from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import { terser } from "rollup-plugin-terser";

export default {
  input: 'src/galton.ts',
  output: {
    file: 'public/bundle.js',
    format: 'es',
  },
  plugins: [ 
      commonjs(), 
      resolve(), 
      typescript(),
      terser(),
      execute('sass --no-source-map --style=compressed sass:public'),
  ],
};
