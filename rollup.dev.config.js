import commonjs from '@rollup/plugin-commonjs';
import execute from 'rollup-plugin-execute';
import resolve from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';

export default {
  input: 'src/galton.ts',
  output: {
    file: 'public/bundle.js',
    format: 'es',
  },
  plugins: [ 
      commonjs(), 
      resolve(), 
      typescript(),
      execute('sass --watch sass:public'),
  ],
};
