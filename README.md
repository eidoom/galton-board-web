# [galton-board-web](https://gitlab.com/eidoom/galton-board-web)

* Live [here](https://eidoom.gitlab.io/galton-board-web/).
* See also the [Python version we use on Raspberry Pis](https://github.com/eidoom/GaltonBoard).
* [Companion post](https://eidoom.gitlab.io/computing-blog/post/galton/)

## Running locally

* [Install Sass](https://sass-lang.com/install), eg. get a [Dart Sass release](https://github.com/sass/dart-sass/releases/) and add it to your path:
```shell
cd ~/programs
wget https://github.com/sass/dart-sass/releases/download/1.26.11/dart-sass-1.26.11-linux-x64.tar.gz
tar -xvzf dart-sass-1.26.11-linux-x64.tar.gz
echo 'export PATH="$HOME/programs/dart-sass:$PATH"' >> ~/.zshrc
```
This is the reference implementation, and the fastest.
Alternatively, just use the [npm](https://www.npmjs.com/package/sass)/[rollup](https://www.npmjs.com/package/rollup-plugin-scss) version (slower to run, but installation looked after).

* Initialise and build web app with
```shell
npm i
```

* Continuous builds with
```shell
npm run dev
```

* Due to [browser security features](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp), must be run from a server with the `http(s)://` protocol, and **not** by directly opening the file with the browser using the `file://` protocol, despite being a static site.

For live reloads, use [`live-server`](https://www.npmjs.com/package/live-server).
Install with
```shell
sudo npm i -g live-server
```
and run with
```shell
live-server public
```
which creates the site at <http://localhost:8080>.

## Technologies

* [Typescript](https://www.typescriptlang.org/)
* [Sass](https://sass-lang.com/)
* [Rollup](https://rollupjs.org/guide/en/)
* [Chart](https://www.chartjs.org/)

## TODO

* Attribute dependencies here
* https://github.com/proteriax/rollup-plugin-ignore for `moment` from https://www.chartjs.org/docs/latest/getting-started/integration.html
    * Doesn't seem to have any effect
* Minify with https://github.com/TrySound/rollup-plugin-terser: done: 603K -> 236K
* Optimise with `wasm`
* Current dimensions match the real Galton board, but we'd get a better binomial distribution if we were in the strongly coupled regime (more scattering)
* Range sliders don't reset when tab is duplicated
